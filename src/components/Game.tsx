import React, { useState } from "react";
import { Sudoku } from "../classes/Sudoku";

export default function Game() {
  const [date, setDate] = useState<Date | undefined>(new Date());
  const [sudoku, setSudoku] = useState<Sudoku>(new Sudoku());
  const [selected, setSelected] = useState<Array<number> | null>(null);

  const select = (i: number, j: number) => {
    if (!sudoku.initial[i][j]) {
      setSelected([i, j]);
    }
  };

  const setValue = (newValue: number) => {
    if (selected && !sudoku.initial[selected[0]][selected[1]]) {
        const i: number = selected[0];
        const j: number = selected[1];
        console.log(i, j, newValue)
        sudoku.setCurrent(i, j, newValue);
        console.log(sudoku.current)
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-6">
          <div className="row board">
            {Array.from({ length: 9 }, (_, n) => n).map((i) => (
              <div key={i} className="col-sm-4 no-padding strong-border">
                <div className="row">
                  {Array.from({ length: 9 }, (_, n) => n).map((j) => (
                    <div
                      key={j}
                      className={getClass(i, j, sudoku.initial[i][j], selected)}
                      onClick={() => select(i, j)}
                    >
                      {sudoku.current[i][j] || "_"}
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="col-sm-6">
          <div className="row">
            {Array.from({ length: 9 }, (_, n) => n + 1).map((i) => (
              <div key={i} className="col-sm-4">
                <button className="large primary" onClick={() => setValue(i)}>
                  {i}
                </button>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

function getClass(
  i: number,
  j: number,
  initial: boolean,
  selected: Array<number> | null
): string {
  let cl: string = "col-sm-4 center ";
  if (initial) {
    cl += "initial ";
  } else {
    cl += "clickable ";
  }
  if ((j === 0 || j === 3 || j === 6) && i !== 0 && i !== 3 && i !== 6) {
    cl += "strong-border-left ";
  }
  if (i >= 3 && (j === 0 || j === 1 || j === 2)) {
    cl += "strong-border-top ";
  }
  if (selected && selected[0] === i && selected[1] === j) {
    cl += "selected ";
  }
  return cl;
}
