import React, { useState } from 'react';
import './App.css';
import Game from './components/Game';
import Menu from './components/Menu';

function App() {
  const [isPlaying, setPlaying] = useState<boolean>(false)

  return (
    <>
    <Menu/>
    <Game />
    </>
  );
}

export default App;