export class Sudoku {
  initial: Array<Array<boolean>>;
  solution: Array<Array<number>>;
  current: Array<Array<number | null>>;
  finished: boolean;
  error: boolean;

  constructor() {
    console.log(this.generate);
    this.initial = this.generate()[0];
    this.solution = this.generate()[1];
    this.current = this.generate()[2];
    this.error = false;
    this.finished = false;
  }

  generate(): any {
    var multi: number[][] = [
      [1, 2, 3],
      [23, 24, 25],
    ];
    //return multi;

    let initial: Array<Array<boolean>> = [];
    let solution: Array<Array<number>> = [];
    let current: Array<Array<number | null>> = [];
    for (let i: number = 0; i < 9; i++) {
      initial[i] = [false, true, true, false, true, true, false, false, false];
      solution[i] = Array.from({ length: 9 }, (_, n) => n + i);
      current[i] = solution[i].map((val, idx) =>
        initial[i][idx] ? solution[i][idx] : null
      );
    }
    return [initial, solution, current];
  }

  setCurrent(i: number, j: number, value: number): void {
    this.current[i][j] = value;
    this.checkCurrent();
  }

  isFinished(): boolean {
    return JSON.stringify(this.solution) === JSON.stringify(this.current);
  }

  hasError(): boolean {
    if (this.finished) return false;
    for (let i: number = 0; i < 9; i++) {
      this.solution[i].map((val, idx) => {
        if (
          this.current[i][idx] !== null &&
          this.current[i][idx] !== this.solution[i][idx]
        ) {
          return true;
        }
      });
    }
    return false;
  }

  checkCurrent(): void {
    this.finished = this.isFinished();
    this.error = this.hasError();
  }
}
